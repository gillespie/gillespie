/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include <vector>
#include <cxxtest/TestSuite.h>
#include "gillespie/gillespie.h"
#define protected public
#include "gillespie/gillespie_list.h"

using namespace gillespie {
class MyTestSuite : public CxxTest::TestSuite
{
public:
	Gillespie *g1;
	Gillespie *g2;
	Event *ev1;
	Event *ev2;

	void setUp() {
		g1 = GillespieList::Instance();
		g2 = GillespieList::Instance();
		ev1 = new EventList( 2.0 );
		ev2 = new EventList( 2.0 );
	}

	void tearDown() {
		g1->reset();
		g2->reset();
	}
	
	void testEvent( void ) {
		TS_ASSERT_EQUALS( ev1->get_rate(), 2.0 );
		ev1->set_rate( 4.0 );
		TS_ASSERT_EQUALS( ev1->get_rate(), 4.0 );
	}

	void testExecuteEvent( void ) {
		TS_ASSERT_EQUALS( ev1->get_rate(), 2.0 );
		ev1->execute();
		TS_ASSERT_EQUALS( ev1->get_rate(), 4.0 );
	}


	void testGillespieInstances( void ) {
		TS_ASSERT_EQUALS( g1, g2 );
	}

	void testGillespieTree_reset( void ) {
		g1->reset();
		g1->add_event( ev1 );
		TS_ASSERT_EQUALS( g1->get_rate(), 2.0 );
		g1->reset();
		TS_ASSERT_EQUALS( g1->get_rate(), 0.0 );
	}

	void testGillespieTree_add_event1( void ) {
		g1->add_event( ev1 );
		TS_ASSERT_EQUALS( g1->get_rate(), 2.0 );
		g1->add_event( ev2 );
		TS_ASSERT_EQUALS( g1->get_rate(), 4.0 );
		ev2->set_rate( 0.0 );
		TS_ASSERT_EQUALS( g1->get_rate(), 2.0 );
		ev1->set_rate( 0.0 );
		TS_ASSERT_EQUALS( g1->get_rate(), 0.0 );
	}

	void testGillespieTree_add_event2( void ) {
		ev1->set_rate(4.0);
		g1->add_event( ev1 );
		TS_ASSERT_EQUALS( g1->get_rate(), 4.0 );
	}

	void testGillepie_del_event( void ) {
		g1->add_event( ev1 );
		TS_ASSERT_EQUALS( g1->get_rate(), 2.0 );
		g1->add_event( ev2 );
		TS_ASSERT_EQUALS( g1->get_rate(), 4.0 );
		g1->del_event( ev1 );
		TS_ASSERT_EQUALS( g1->get_rate(), 2.0 );
		//GillespieList specific
		GillespieList *gl = static_cast<GillespieList*>( g1 );
		TS_ASSERT_EQUALS( gl->events.size(), 1 );
		//!specific
		g1->del_event( ev2 );
		TS_ASSERT_EQUALS( g1->get_rate(), 0.0 );
		//GillespieList specific
		TS_ASSERT_EQUALS( gl->events.size(), 0 );
		//!specific
	}

	/*
	 * Randomness is involved, but trying to test by setting one rate to 0.0, i.e. it should never happen
	 */

	void testGillespieTree_execute_an_event1( void ) {
		ev2->set_rate(0.0);
		g1->add_event( ev1 );
		g1->add_event( ev2 );
		g1->execute_an_event();
		TS_ASSERT_EQUALS( ev1->get_rate() , 4.0 ); //This one has executed
		TS_ASSERT_EQUALS( ev2->get_rate() , 0.0 );
	}
	
	void testGillespieTree_execute_an_event2( void ) {
		ev1->set_rate(0.0);
		ev2->set_rate(2.0);
		g2->add_event( ev1 );
		g2->add_event( ev2 );
		TS_ASSERT_EQUALS( g2->get_rate(), 2.0 );
		g2->execute_an_event();
		TS_ASSERT_EQUALS( g2->get_rate(), 4.0 );
		TS_ASSERT_EQUALS( ev1->get_rate(), 0.0 ); 
		TS_ASSERT_EQUALS( ev2->get_rate(), 4.0 ); //This one has executed
	}

	/*
	 * Due to the random number generator this one is hard to test properly.
	 * At the moment we only test that the method exists
	 */

	void testGillespieTree_time_till_next_event( void ) {
		g1->add_event( ev1 );
		TS_ASSERT_THROWS_NOTHING( g1->time_till_next_event(); )
		//double result=0;
		//for (int i=0; i<10000;i++ ) {
		//	result = result+ g1->time_till_next_event();
		//}
		//std::cout << std::endl << "Should be around 0.5: " << result/10000 << std::endl;
	}

};
}
