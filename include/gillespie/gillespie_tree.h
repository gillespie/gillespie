#ifndef GILLESPIE_TREE_H
#define GILLESPIE_TREE_H

#include <vector>

#include "gillespie/binary_tree.h"
#include "gillespie/gillespie.h"


namespace gillespie {

//class GillespieTree;

/**
 * \brief This is the parent eventtree class used in GillespieTree
 *
 * Users of the GillespieTree will want to define their own events and inherit from
 * this class.
 *
 * Example:
 * @verbatim
class DeathEvent : public EventTree
{
    public:
        Population* pPop;
        DeathEvent(  ) : EventTree( 1.0 ) {
            pPop = Population::Instance();
        }
        virtual void execute() {
            pPop->death();
        }
};
 @endverbatim
 * See the examples for more information
 */

class EventTree : public Event, public std::enable_shared_from_this<EventTree> {
	public:
		TreeNode tree_leaf;

		EventTree( double rate_value );
		~EventTree() {};
		virtual double get_rate() { return( tree_leaf.rate ); }
		virtual void set_rate(double new_rate) { tree_leaf.set_rate( new_rate ); }
		virtual void execute();
};

class MyEventTree : public EventTree
{
	public:
		double data;
		MyEventTree( double rate, double orig_data ) : EventTree( rate ) { data = orig_data; }
		virtual void execute() { data = get_rate(); }
};

/**
 * \brief Gillespie implementation with a binary tree as its central data structure
 *
 * Should be fast for cases where rates are relatively stable. Adding and deleting
 * events is reasonably fast although also slower than in the \ref GillespieList
 * It is a lot faster in choosing the event to execute though, since instead of 
 * (potentially) having to traverse the whole event list it only has to traverse
 * the tree from top to bottom. 
 *
 * At the moment it is implemented as an instance class, such that the same instance/
 * object is used across the whole project. I am thinking about changing this and 
 * let me know if you have a good use case for the standard way.
 *
 * Example code:
 * @verbatim
 double time = 0;
 Gillespie *gill = GillespieTree::Instance();
 std::shared_ptr<Event> an_event = std::shared_ptr<DeathEvent>( new DeathEvent() );
 std::shared_ptr<Event> another_event = std::shared_ptr<DeathEvent>( new DeathEvent() );
 gill->add_event( an_event );
 gill->add_event( another_event );
 while (true) {
    time += gill->time_till_next_event();
    gill->execute_an_event();
 }
 @endverbatim
 */
class GillespieTree : public Gillespie {
	public:
		static GillespieTree* Instance();
		~GillespieTree();
		virtual void add_event( std::shared_ptr<Event> event );
		virtual void del_event( std::shared_ptr<Event> event );
		virtual std::shared_ptr<Event> get_next_event();
		virtual void reset();
		virtual double get_rate() { return( binary_tree.get_rate() ); }
		virtual size_t no_of_events();
	protected:
		static GillespieTree* pInstance;
		BinaryTree<TreeNode> binary_tree;

		void walk_tree( double prob_rate );
		
		GillespieTree();
};
}
#endif
