/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010-2014, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#ifndef GILLESPIE_H
#define GILLESPIE_H

#include <random>
#include <iostream>
#include <memory>

namespace gillespie {

/**
 * \brief This is the parent event class. Any event needs to implement these methods
 *
 * Specific implementations of the gillespie algorithm should inherit this class.
 * Normal users should not inherit this class directly and instead use 
 * \ref EventList or \ref EventTree depending on the version they want to use.
 */
    //This is a dummy Event class, that specific implementations inherit from
class Event {
	public:
		Event() {}
		virtual ~Event() {}
		virtual double get_rate() = 0;
		virtual void set_rate(double new_rate) = 0;
		virtual void execute() = 0;
};

/**
 * \brief This is the parent gillespie class. Any Gillespie child needs to implement these methods
 *
 * Specific implementations of the gillespie algorithm should inherit this class.
 * Normal users should not use this class directly and instead use 
 * \ref GillespieList or \ref GillespieTree depending on the version they want to use.
 *
 * Currently, the Gillespie classes are all Instance classes, so that the Gillespie
 * algorithm can be used from anywhere in your program. This might change in the future
 * because it sometimes let to problems and surprising behaviour. If you think it
 * should be changed please contact me to make your case :)
 *
 * Example code:
 * @verbatim
 double time = 0;
 Gillespie *gill = Gillespie[List|Tree]::Instance();
 gill->add_event( an_event );
 gill->add_event( another_event );
 while (true) {
    time += gill->time_till_next_event();
    gill->execute_an_event();
 }
 @endverbatim
 */
class Gillespie {
	public:
		std::mt19937 generator;

		virtual void add_event( std::shared_ptr<Event> event ) = 0;
		virtual void del_event( std::shared_ptr<Event> event ) = 0;

		/**
		 * \brief Get the next event
		 *
		 * This is where the performance can differ greatly dependent
		 * on the implementation.
		 */
		virtual std::shared_ptr<Event> get_next_event() = 0;

		/**
		 * \brief Get and immediately the next event
		 *
		 * Returns the executed event.
		 */
		std::shared_ptr<Event> execute_an_event();

		virtual void reset() = 0;
		virtual void set_rate( double new_rate ) { rate = new_rate; }
		virtual double get_rate() {return rate;}

		/**
		 * \brief Returns the time till the next event takes place
		 *
		 * Used to keep track of what time it is now :)
		 */
		double time_till_next_event();

		/**
		 * \brief Returns total number of events
		 */
		virtual size_t no_of_events() = 0;
	protected:
		double rate;
		Gillespie();
	private:
};
}
#endif
