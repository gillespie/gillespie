/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#ifndef GILLESPIE_LIST_H
#define GILLESPIE_LIST_H

#include <list>
#include "gillespie/gillespie.h"

namespace gillespie {

/**
 * \brief This is the parent eventlist class used in GillespieList
 *
 * Users of the GillespieList will want to define their own events and inherit from
 * this class.
 *
 * Example:
 * @verbatim
class DeathEvent : public EventList
{
    public:
        Population* pPop;
        DeathEvent(  ) : EventList( 1.0 ) {
            pPop = Population::Instance();
        }
        virtual void execute() {
            pPop->death();
        }
};
 @endverbatim
 * See the examples for more information
 */
class EventList : public Event {
	public:
		EventList( double event_rate );
		~EventList() {}

		void add_to_gillespie( Gillespie* gillespie );
		void del_from_gillespie();
		virtual void set_rate( double new_rate );
		virtual double get_rate( ) {return rate;}
		virtual void execute();
	protected:
		double rate;
		Gillespie* pGillespie;
};

/**
 * \brief Gillespie implementation with a list as its central data structure
 *
 * Should be fast for cases where rates are updated a lot, and events are added and
 * deleted often. For example in a density dependent birth death process the birth 
 * and/or death rates of all individuals need to be updated everytime an individual
 * dies or gets born. In such a case this implementation is faster than the Tree
 * implementation.
 *
 * At the moment it is implemented as an instance class, such that the same instance/
 * object is used across the whole project. I am thinking about changing this and 
 * let me know if you have a good use case for the standard way.
 *
 * Example code:
 * @verbatim
 double time = 0;
 Gillespie *gill = GillespieList::Instance();
 std::shared_ptr<Event> an_event = std::shared_ptr<DeathEvent>( new DeathEvent() );
 std::shared_ptr<Event> another_event = std::shared_ptr<DeathEvent>( new DeathEvent() );
 gill->add_event( an_event );
 gill->add_event( another_event );
 while (true) {
    time += gill->time_till_next_event();
    gill->execute_an_event();
 }
 @endverbatim
 */
class GillespieList : public Gillespie {
	public:
		static GillespieList* Instance();
		~GillespieList();
		virtual void add_event( std::shared_ptr<Event> event );
		virtual void del_event( std::shared_ptr<Event> event );
		virtual void reset();
		virtual std::shared_ptr<Event> get_next_event();
		virtual size_t no_of_events();
	protected:
		static GillespieList* pInstance;
		std::list< std::shared_ptr<Event> > events;

		GillespieList();

};
}
#endif
