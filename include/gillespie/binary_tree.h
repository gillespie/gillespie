/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010-2014, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/
#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <random>
#include <vector>

#include <gillespie/gillespie.h>

namespace gillespie {

/* \file binary_tree.h
*/

/**
 * \brief General node for the binary tree, containing pointers to its parent and its
 * "daughter"
 *
 * Endnode also have a pointer to an Event
 */
class TreeNode {
	public:
		double rate;
		TreeNode *left;
		TreeNode *right;
		TreeNode *parent;
		bool leaf;
		Event *pEvent;

		TreeNode();
		~TreeNode() {}
		void draw();
		virtual void set_rate( double new_rate );
		virtual void change_rate( double delta );
};

/**
 * \brief Binary tree as used for the gillespie algorithm
 * Leaves contain links to an event: \ref EventTree
 *
 * Currently, leaves are added randomly, to keep the tree roughly balanced,
 * which seems to work pretty well.
 * 
 * \future Add a rebalance function to rebalance the tree once in a while
 */

template <class T>
class BinaryTree {
	public:
		T *root_node;
		T *current_node;

		BinaryTree();
		~BinaryTree() {}
		void reset();
		double get_rate();
		void add_node( T *node, T *parent, bool left );
		void del_node( T *node );
		void add_leaf_node( T *node );
		double average_length();
		double variance_length();
		int number_of_leaves();
		int number_of_leaves( int start, T *node );
		void draw();
	protected:
		std::mt19937 generator;
		std::uniform_real_distribution<double> runif_01 = 
			std::uniform_real_distribution<double>( 0, 1 );
		T *random_end_node( );
		void random_walk( double probabitility );
		void draw_from_node( T *node );
		void count_length_from( int length, T *node, std::vector<int> &lengths );
};


template <class T>
BinaryTree<T>::BinaryTree() {
	// Setup random number generator
	generator.seed( time(0) );

	root_node = new T();
	current_node = root_node;
}

template <class T>
void BinaryTree<T>::reset() {
	root_node = new T();
	current_node = root_node;
}

template <class T>
double BinaryTree<T>::get_rate() {
	return( root_node->rate );
}

/*
 * Helper function to correctly add new nodes
 */
template <class T>
void BinaryTree<T>::add_node( T *node, T *parent, bool left ) {
	node->parent = parent;
	if (left) {
		parent->left = node;
	} else {
		parent->right = node;
	}
	//Hack to make sure the rates are updated correctly
	double tmp_rate = node->rate;
	node->rate = 0;
	node->set_rate( tmp_rate );
}

template <class T>
void BinaryTree<T>::del_node( T *node ) {
	T *pParent = node->parent;
	if (pParent->left == node) {
		pParent->left = 0;
	} else {
		pParent->right = 0;
	}
	pParent->set_rate( pParent->rate - node->rate );
}

/*
 * Adds nodes randomly, to keep the tree relatively balanced
 */
template <class T>
void BinaryTree<T>::add_leaf_node(T *node) {
	current_node = root_node;
	T *end_node = random_end_node( );
	if (!end_node->leaf) {
		//Do another draw.. Could be prevented, by changing the random walk etc 
        //and returning the rate in some way
		//But this is easier and shouldn't be much slower
		double rand = runif_01( generator );
		if (rand <=0.5) {
			if (end_node->left == 0) {
				add_node( node, end_node, true );
			} else {
				add_node( node, end_node, false );
			}
		} else {
			if (end_node->right == 0) {
				add_node( node, end_node, false );
			} else {
				add_node( node, end_node, true );
			}	
		} 
	}	else {
		// End node is a leaf. Need to detach end node, 
        // insert new node and add both leafs (new and old)
		T *pParent = end_node->parent;
		T *pNewNode = new T();
		
		del_node( end_node );
		if (pParent->left == 0) {
			add_node( pNewNode, pParent, true );
		} else {
			add_node( pNewNode, pParent, false );
		}
		
		add_node( end_node, pNewNode, true );
		add_node( node, pNewNode, false );
	}
}

template <class T>
T *BinaryTree<T>::random_end_node() {
	double rand = runif_01( generator );
	random_walk( rand );
	return( current_node );
}

template <class T>
void BinaryTree<T>::random_walk( double probability ) {
	T *next_node;
	if (probability <= 0.5) {
		next_node = current_node->left;
		probability = probability*2;
	} else {
		next_node = current_node->right;
		probability = (probability-0.5)*2;
	}
	if (next_node != 0) {
		current_node = next_node;
		random_walk( probability );
	}
}

template <class T>
double BinaryTree<T>::average_length() {
	std::vector<int> lengths;
	count_length_from( 0, root_node, lengths );
	int sum = 0;
	for (int i=0; i<lengths.size(); i++) {
		sum = sum + lengths[i];
	}
	return( float(sum)/lengths.size() );
}

/* 
 * At the moment this function recreates the lengths vector.. Could be more efficient, but function should only be used rarely.
 */
template <class T>
double BinaryTree<T>::variance_length() {
	std::vector<int> lengths;
	count_length_from( 0, root_node, lengths );
	double sum = 0;
	double average = average_length();
	for (int i=0; i<lengths.size(); i++) {
		sum = sum + (lengths[i]-average)*(lengths[i]-average);
	}
	return( float(sum)/lengths.size() );
}

template <class T>
int BinaryTree<T>::number_of_leaves() {
	return( number_of_leaves( 0, root_node ) );
}

template <class T>
int BinaryTree<T>::number_of_leaves(int start, T *node) {
	if (node->leaf) {
		start++;
	} else {
		if (node->left != 0)
			start = number_of_leaves( start, node->left );
		if (node->right != 0)
			start = number_of_leaves( start, node->right );
	}
	return(start);
}

template <class T>
void BinaryTree<T>::count_length_from( int length, T *node, std::vector<int> &lengths ) {
	if (node->left !=0) {
		count_length_from( length+1, node->left, lengths );
	} 
	if (node->right !=0) {
		count_length_from( length+1, node->right, lengths );
	} 
	if (node->right == 0 && node->left == 0) {
		lengths.push_back( length );
	}
}

template <class T>
void BinaryTree<T>::draw( void ) {
	draw_from_node( root_node );
}

template <class T>
void BinaryTree<T>::draw_from_node( T *node ) {
	std::cout << "Pointer " << node << std::endl;
	std::cout << "Parent " << node->parent << std::endl;
	std::cout << "left " << node->left << std::endl;
	std::cout << "right " << node->right << std::endl;
	std::cout << "leaf " << node->leaf << std::endl;
	std::cout << "rate " << node->rate << std::endl;
	if (node->left != 0) {
		draw_from_node( node->left ); }
	if (node->right != 0) {
		draw_from_node( node->right ); }
}

}


#endif
