/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include <vector>
#include <iostream>

#include "exponential_growth.h"

Individual::Individual( std::shared_ptr<Event> pBirth, 
		std::shared_ptr<Event> pDeath ) {
	pBirthEvent = pBirth;
	pDeathEvent = pDeath;
}

Individual::~Individual() {}

void Individual::set_birth_rate( double brate ) {
	pBirthEvent->set_rate( brate );
}
double Individual::get_birth_rate() {
	return pBirthEvent->get_rate();
}
void Individual::set_death_rate( double drate ) {
	pDeathEvent->set_rate( drate );
}
double Individual::get_death_rate() {
	return pDeathEvent->get_rate();
}

Population* Population::pInstance = 0;

Population* Population::Instance() {
	if (pInstance == 0) { // is it the first call?
		pInstance = new Population(); // create sole instance
	}
	return pInstance; // address of sole instance
}

Population::Population() {
	pGillespie = 0;
}

Population::~Population() {
	if (pInstance != 0) {
		delete pInstance;
	}
}

int Population::size() {
	return( individuals.size() );
}

void Population::birth( std::shared_ptr<Event> birth, 
		std::shared_ptr<Event> death ) {
	Individual ind = Individual( birth, death );
	individuals.push_back( ind );
	if (pGillespie == 0) 
		throw;
	pGillespie->add_event( ind.pBirthEvent );
	pGillespie->add_event( ind.pDeathEvent );
}

void Population::death() {
	if (size() > 0) {
		Individual ind = individuals[individuals.size()-1];
		if (pGillespie == 0) 
			throw;
		pGillespie->del_event( ind.pBirthEvent );
		pGillespie->del_event( ind.pDeathEvent );
		individuals.pop_back();
	}
}

Individual Population::get_individual( int id ) {
	return individuals[id-1];
};

