/*
	 -------------------------------------------------------------------

	 Copyright (C) 2010, Edwin van Leeuwen

	 This file is part of Gillespie.

	 Gillespie is free software; you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation; either version 3 of the License, or
	 (at your option) any later version.

	 Gillespie is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

	 -------------------------------------------------------------------
	 */
#include <iostream>
#include <stdio.h>
#include <time.h>

#include "realtimeplot/plot.h"

#include "gillespie/gillespie_list.h"
#include "exponential_growth.h"

using namespace realtimeplot;

/**
 * \file density_dependent_death_list.cc
 *
 * This example is slightly silly, since I'm modelling each individual separately.
 * It would have been much more effective to just model the total population, with
 * the event rates dependent on the density of the one population (resulting in only
 * two events in total.
 * It's done in this way for a number of reasons:
 * 1) It allows one to use it as a simple benchmark
 * 2) To show the possibilities
 * 3) Often one needs to keep track of each individual, for example when one also 
 * 		wants to keep track of an individuals position
 */

class DeathEvent : public EventList
{
	public:
		Population* pPop;
		DeathEvent( double rate ) : EventList( rate ) {
			pPop = Population::Instance();
		}
		virtual void execute() {
			pPop->death();
		}
};

class BirthEvent : public EventList
{
	public:
		Population* pPop;
		double death_rate;


		BirthEvent(double birthrate, double deathrate) : EventList( birthrate ) {
			pPop = Population::Instance();
			death_rate = deathrate;
		}
		virtual void execute() {
			pPop->birth( std::shared_ptr<BirthEvent>( 
						new BirthEvent( this->get_rate(), death_rate ) ), 
					std::shared_ptr<DeathEvent>( new DeathEvent( death_rate ) ) );
		}
};


int main( void ) {
	Population &pop = *Population::Instance();
	Gillespie &gill = *GillespieList::Instance();
	pop.pGillespie = &gill;


	int steps = 5000;
	double base_death_rate = 0.01; //death_rate = base_death_rate*pop.size()
	double death_rate = base_death_rate*10;
	double t = 0;

	//Initialize population
	for (int i=0; i<10; i++) {
		pop.birth( std::shared_ptr<BirthEvent>( new BirthEvent( 2.0, death_rate ) ), std::shared_ptr<DeathEvent>( new DeathEvent( death_rate ) ) );
	}

	PlotConfig config = PlotConfig();
	config.max_x = 10;
	config.min_y = 0;
	config.max_y = 300;
	config.xlabel = "Time";
	config.ylabel = "Density";
	config.title = "Density dependent growth";
	config.nr_of_ticks = 5;
	Plot pl = Plot( config );


	//Start stochastic progress
	double start_time = time(0);
	for (int i=0; i<steps; i++) {
		t = t + gill.time_till_next_event();
		gill.execute_an_event();
		//update new death rates
		death_rate = base_death_rate*pop.size();
		for (int j=0; j<pop.size(); j++) {
			pop.get_individual(j+1).set_death_rate( death_rate  );
		}
		//pl.point( t, pop.size() );
		pl.line_add( t, pop.size() );
	}
	std::cout << "Run time: " << (time(0) - start_time) << std::endl;
	pl.save("density_dependent.png");
	return( 0 );
}
