#include <iostream>
#include <stdio.h>
#include <time.h>

#include "realtimeplot/plot.h"

#include "gillespie/gillespie_tree.h"
#include "exponential_growth.h"

using namespace realtimeplot;

/**
 * \file density_dependent_death_tree.cc
 *
 * This example is slightly silly, since I'm modelling each individual separately.
 * It would have been much more effective to just model the total population, with
 * the event rates dependent on the density of the one population (resulting in only
 * two events in total.
 * It's done in this way for a number of reasons:
 * 1) It allows one to use it as a simple benchmark
 * 2) To show the possibilities
 * 3) Often one needs to keep track of each individual, for example when one also 
 * 		wants to keep track of an individuals position
 */

class DeathEvent : public EventTree
{
    public:
        Population* pPop;
        DeathEvent( double rate ) : EventTree( rate ) {
            pPop = Population::Instance();
        }
        virtual void execute() {
            pPop->death();
        }
};

class BirthEvent : public EventTree
{
    public:
        Population* pPop;
        double death_rate;


        BirthEvent(double birthrate, double deathrate) : EventTree( birthrate ) {
            pPop = Population::Instance();
            death_rate = deathrate;
        }
        virtual void execute() {
            pPop->birth( std::shared_ptr<BirthEvent>( new BirthEvent( this->get_rate(), 
										death_rate ) ), 
                    std::shared_ptr<DeathEvent>( new DeathEvent( death_rate ) ) );
        }
};


int main( void ) {
	Population &pop = *Population::Instance();
	Gillespie &gill = *GillespieTree::Instance();
	pop.pGillespie = &gill;


	int steps = 100000;
	double base_death_rate = 0.001; //death_rate = base_death_rate*pop.size()
	double death_rate = base_death_rate*10;
	double t = 0;
	
	//Initialize population
	for (int i=0; i<10; i++) {
		pop.birth( std::shared_ptr<BirthEvent>( new BirthEvent( 2.0, death_rate ) ), std::shared_ptr<DeathEvent>( new DeathEvent( death_rate ) ) );
	}

    PlotConfig config = PlotConfig();
    config.max_x = 20;
    config.min_y = 0;
    config.max_y = 3000;
    Plot pl = Plot( config );
    

	//Start stochastic progress
	double start_time = time(0);
	for (int i=0; i<steps; i++) {
		t = t + gill.time_till_next_event();
		gill.execute_an_event();
		//update new death rates
		death_rate = base_death_rate*pop.size();
		for (int j=0; j<pop.size(); j++) {
			pop.get_individual(j+1).set_death_rate( death_rate  );
		}
        pl.point( t, pop.size() );
	}
	std::cout << "Run time: " << (time(0) - start_time) << std::endl;
	return( 0 );
}
