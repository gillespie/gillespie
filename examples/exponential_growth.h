/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#ifndef EXPONENTIAL_GROWTH_H
#define EXPONENTIAL_GROWTH_H
#include <vector>
#include "gillespie/gillespie.h"
using namespace gillespie;
class Individual {
	public:
		std::shared_ptr<Event> pBirthEvent;
		std::shared_ptr<Event> pDeathEvent;
		
		//
		Individual() {};
		Individual( std::shared_ptr<Event> pBirth, 
				std::shared_ptr<Event> pDeath );
		~Individual();
		void set_birth_rate( double brate );
		double get_birth_rate();
		void set_death_rate( double drate );
		double get_death_rate();
};

class Population {
	public:
		Gillespie *pGillespie;

		static Population* Instance( );
		~Population();
		int size();
		void birth( std::shared_ptr<Event> birth, std::shared_ptr<Event> death );
		void death();
		Individual get_individual( int id );
	protected:
		std::vector<Individual> individuals;

		Population();
	private:
		static Population* pInstance;
};
#endif
