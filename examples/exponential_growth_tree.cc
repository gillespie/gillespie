/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/
/**
 * \file exponential_growth_tree.cc
 *
 * This example is slightly silly, since I'm modelling each individual separately.
 * It would have been much more effective to just model the total population, with
 * the event rates dependent on the density of the one population (resulting in only
 * two events in total.
 * It's done in this way for a number of reasons:
 * 1) It allows one to use it as a simple benchmark
 * 2) To show the possibilities
 * 3) Often one needs to keep track of each individual, for example when one also 
 * 		wants to keep track of an individuals position
 */

#include "gillespie/gillespie_tree.h"
#include "exponential_growth.h"
#include <realtimeplot/plot.h>
#include <time.h>
#include <iostream>

using namespace gillespie;

class DeathEvent : public EventTree
{
	public:
		Population* pPop;
		DeathEvent(  ) : EventTree( 1.0 ) {
			pPop = Population::Instance();
		}
		virtual void execute() {
			pPop->death();
		}
};

class BirthEvent : public EventTree
{
	public:
		Population* pPop;

		BirthEvent() : EventTree( 2.0 ) {
			pPop = Population::Instance();
		}
		virtual void execute() {
			pPop->birth( std::shared_ptr<BirthEvent>( new BirthEvent() ), 
				std::shared_ptr<DeathEvent>( new DeathEvent() ) );
		}
};

int main( void ) {
	Population &pop = *Population::Instance();
	Gillespie &gill = *GillespieTree::Instance();
	pop.pGillespie = &gill;
	int steps = 50000;
	double t = 0;

	//Initialize population
	for (int i=0; i<10; i++) {
		pop.birth( std::shared_ptr<BirthEvent>( new BirthEvent() ), 
				std::shared_ptr<DeathEvent>( new DeathEvent() ) );
	}

	//Plotting
	realtimeplot::PlotConfig plot_config = realtimeplot::PlotConfig();
	plot_config.max_x = 6;
	plot_config.max_y = 4000;
	realtimeplot::Plot pl = realtimeplot::Plot( plot_config );

	//Start stochastic progress
	double start_time = time(0);
	for (int i=0; i<steps; i++) {
		t = t + gill.time_till_next_event();
		gill.execute_an_event();
		pl.point( t, pop.size() );
	}
	std::cout << "Run time: " << (time(0) - start_time) << std::endl;
	sleep( 10 );
	return( 0 );
}
