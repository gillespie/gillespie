/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010-2014, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/
#include <iostream>
#include <stdio.h>
#include <time.h>

#include "gillespie/binary_tree.h"
#include "gillespie/gillespie_tree.h"

namespace gillespie {
/*
 * This is the base event class 
 */

EventTree::EventTree( double rate_value ) : Event() {
	tree_leaf = TreeNode();
	tree_leaf.pEvent = this;
	tree_leaf.leaf = true;
	set_rate( rate_value );
}

void EventTree::execute() { std::cout << "Dummy executing" << std::endl; }

GillespieTree* GillespieTree::pInstance = 0;

GillespieTree* GillespieTree::Instance() {
	if (pInstance == 0) { // is it the first call?
		pInstance = new GillespieTree(); // create sole instance
	}
	return pInstance; // address of sole instance
}

GillespieTree::GillespieTree() : Gillespie() {
	binary_tree = BinaryTree<TreeNode>();
	// Setup random number generator
	generator.seed( time(0) );
}

GillespieTree::~GillespieTree() {
	/*if (pInstance != 0) {
		delete pInstance;
	}*/
}

void GillespieTree::reset() {
	binary_tree.reset();
}
/*
 * std::shared_ptr<Base> base (new Derived());
std::shared_ptr<Derived> derived =
               std::dynamic_pointer_cast<Derived> (base);
 */
void GillespieTree::add_event( std::shared_ptr<Event> pEventTree ) {
	EventTree *et = 
		dynamic_cast<EventTree*>( pEventTree.get() );
	binary_tree.add_leaf_node( &(et->tree_leaf) );
}

void GillespieTree::del_event( std::shared_ptr<Event> pEventTree ) {
	EventTree *et = 
		dynamic_cast<EventTree*>( pEventTree.get() );
	binary_tree.del_node( &(et->tree_leaf) );
}

std::shared_ptr<Event> GillespieTree::get_next_event() {
	std::uniform_real_distribution<double> runif( 0, get_rate() );
	double prob_rate = runif( generator );

	binary_tree.current_node = binary_tree.root_node;
	walk_tree( prob_rate );

	EventTree *et = 
		dynamic_cast<EventTree*>( binary_tree.current_node->pEvent );

	return et->shared_from_this();
}

void GillespieTree::walk_tree( double prob_rate ) {
	if (!binary_tree.current_node->leaf) {
		double left_rate = 0;
		if (binary_tree.current_node->left != 0) {
			left_rate = binary_tree.current_node->left->rate;
		}
		if (prob_rate <= left_rate) {
			binary_tree.current_node = binary_tree.current_node->left;
		} else {
			prob_rate = prob_rate - left_rate;
			binary_tree.current_node = binary_tree.current_node->right;
		}
		walk_tree( prob_rate );
	}
}

size_t GillespieTree::no_of_events() {
	return binary_tree.number_of_leaves();
}

}
