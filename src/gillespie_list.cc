/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include <iostream>
#include "gillespie/gillespie_list.h"

namespace gillespie {

EventList::EventList(double event_rate) : Event() {
	pGillespie = 0;
	rate = event_rate;
}

void EventList::add_to_gillespie( Gillespie* gillespie ) {
	pGillespie = gillespie;
}
void EventList::del_from_gillespie( ) {
	pGillespie = 0;
}

void EventList::set_rate( double new_rate ) {
	if (pGillespie != 0) {
		pGillespie->set_rate(pGillespie->get_rate()-rate+new_rate);
	}
	rate = new_rate;
}

void EventList::execute() {
	//Dummy action for testing purposes
	set_rate( 4.0 );
}

GillespieList* GillespieList::pInstance = 0;

GillespieList* GillespieList::Instance() {
	if (pInstance == 0) { // is it the first call?
		pInstance = new GillespieList(); // create sole instance
	}
	return pInstance; // address of sole instance
}

GillespieList::GillespieList() : Gillespie () {}
GillespieList::~GillespieList() {
	/*if (pInstance != 0) {
		delete pInstance;
	}*/
}


void GillespieList::add_event( std::shared_ptr<Event> event ) {
	events.push_back( event );
	EventList *el = dynamic_cast<EventList*>(event.get());
	el->add_to_gillespie( pInstance );
	rate = rate + event->get_rate();
}

void GillespieList::del_event( std::shared_ptr<Event> event ) {
	events.remove( event );
	EventList *el = dynamic_cast<EventList*>(event.get());
	el->del_from_gillespie();
	rate = rate - event->get_rate();
}

void GillespieList::reset() {
	rate = 0;
	events.clear();
}

std::shared_ptr<Event> GillespieList::get_next_event() {
	//can probably speed this up, by starting from the back if prob>0.5
	std::uniform_real_distribution<double> runif( 0, rate );
	double prob = runif( generator );
	double sum = 0;


	if (prob<=0.5*rate) {
		std::list<std::shared_ptr<Event> >::iterator i;
		for(i=events.begin(); i != events.end(); ++i) {
			sum = sum + (*i)->get_rate();
			if (sum >= prob) {
				return (*i);
			}
		}
	} else {
		prob = rate-prob;
		std::list<std::shared_ptr<Event> >::reverse_iterator i;
		for(i=events.rbegin(); i != events.rend(); ++i) {
			sum = sum + (*i)->get_rate();
			if (sum >= prob) {
				return (*i);
			}
		}
	}
}

size_t GillespieList::no_of_events() {
	return events.size();
}

}
