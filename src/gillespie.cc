/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010-2014, Edwin van Leeuwen
  
  This file is part of Gillespie.
  
  Gillespie is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  Gillespie is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Gillespie. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include "gillespie/gillespie.h"

namespace gillespie {

Gillespie::Gillespie() {
	generator.seed( time(0) );
}

double Gillespie::time_till_next_event() {
	if (get_rate() <= 0) {
		std::cout << "Gillespie rate: " << get_rate() << std::endl;
		throw;
	}
	std::exponential_distribution<double> rexp( get_rate() );
	return( rexp( generator ) );
}

std::shared_ptr<Event> Gillespie::execute_an_event() {
	std::shared_ptr<Event> pEvent = this->get_next_event();
	pEvent->execute();
	return pEvent;
}

}
