#include <time.h>
#include <iostream>
#include <vector>

#include "gillespie/binary_tree.h"

namespace gillespie {

TreeNode::TreeNode( ) {
	left = 0;
	right = 0;
	parent = 0;
	rate = 0;
	leaf = false;
}

void TreeNode::set_rate( double new_rate ) {
    change_rate( new_rate-rate );
}

void TreeNode::change_rate( double delta ) {
    if (parent != 0) {
        parent->change_rate( delta );
    }
    rate += delta;
}

void TreeNode::draw() {
	std::cout << "Pointer " << this << std::endl;
	std::cout << "Parent " << this->parent << std::endl;
	std::cout << "left " << this->left << std::endl;
	std::cout << "right " << this->right << std::endl;
	std::cout << "leaf " << this->leaf << std::endl;
	std::cout << "rate " << this->rate << std::endl;
}

}
